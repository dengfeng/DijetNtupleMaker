#!/bin/bash

echo "Hello, world"

TESTFILE=""
NEVENTS=-1
ISDATA=false
ISSIG=false
ISBKG=false
while getopts dsbf:n:e: option
do 
case "${option}"
in
f) TESTFILE=${OPTARG};;
d) ISDATA=true;;
s) ISSIG=true;;
b) ISBKG=true;;
n) NEVENTS=${OPTARG};;
e) EOSREPO=${OPTARG};;
esac
done


export ATLAS_LOCAL_ROOT_BASE=/home/atlas/
mkdir /usr/multib/run && cd /usr/multib/run && echo $(pwd) && ls -ltrh

#EOSREPO="root://eoshome.cern.ch///eos/user/d/dibijet/DAOD"
TESTDIR="localTest"

if [ $ISDATA = true ]; then
    echo "Why are you recasting Data?"
    exit 0
elif [ $ISSIG = true ]; then
    CONFIGFILE="/usr/multib/src/MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_signal.py"
elif [ $ISBKG = true ]; then
    echo "Why are you recasting Background?"
    exit 0
fi

echo "Config file: ${CONFIGFILE}"

source /usr/multib/build/${AnalysisBase_PLATFORM}/setup.sh

if [ $ISDATA = true ]; then
    echo "Why are you recasting Data? (You should never reach this point)"
    exit 1
else
    echo xAH_run.py --config ${CONFIGFILE} --files ${EOSREPO}/${TESTFILE} --scanXRD --isMC --submitDir ${TESTDIR} --nevents ${NEVENTS} direct
    xAH_run.py --config ${CONFIGFILE} --files ${EOSREPO}/${TESTFILE} --scanXRD --isMC --submitDir ${TESTDIR} --nevents ${NEVENTS} direct
fi

ls -ltrh
ls -ltrh ${TESTDIR}

if [ -f ${TESTFILE} ]; then
    rm -r ${TESTFILE}
fi

echo "That's all, folks!"

