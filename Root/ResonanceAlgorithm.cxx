#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "AthContainers/ConstDataVector.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include <MultiBTaggedAlgo/ResonanceAlgorithm.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;
// this is needed to distribute the algorithm to the workers
ClassImp(ResonanceAlgorithm)

ResonanceAlgorithm :: ResonanceAlgorithm () :
  m_cutflowHist(0),
  m_cutflowHistW(0),
  m_treeStream("tree")
{

/////////////////////////////////////////////////////////////////////////
// Here you put any code for the base initialization of variables,     //
// e.g. initialize all pointers to 0.  Note that you should only put   //
// the most basic initialization here, since this method will be       //
// called on both the submission and the worker node.  Most of your    //
// initialization code will go into histInitialize() and               //
// initialize().						       //
/////////////////////////////////////////////////////////////////////////

  ANA_MSG_INFO("ResonanceAlgorithm() : Calling constructor");

  m_inJetContainerName       = "";
  m_inputAlgo                = "";
  m_allJetContainerName      = "";
  m_allJetInputAlgo          = "";
  m_msgLevel                 = MSG::INFO;
  m_useCutFlow               = true;
  m_writeTree                = true;
  m_MCPileupCheckContainer   = "AntiKt4TruthJets";
  m_leadingJetPtCut          = 400e3;
  m_subleadingJetPtCut       = 50e3;
  m_jetMultiplicity          = 2;
  m_yStarCut                 = -1; //Off by default
  m_truthLevelOnly           = false;
  m_eventDetailStr           = "truth pileup";
  m_trigDetailStr            = "";
  m_jetDetailStr             = "kinematic clean energy truth flavorTag";
  m_jetDetailStrSyst         = "kinematic clean energy";
  m_doBtag                   = true;
  m_bTagWPNames              = "FixedCutBEff_85";
}

EL::StatusCode  ResonanceAlgorithm :: configure ()
{
  ANA_MSG_INFO("configure() : Configuring ResonanceAlgorithm Interface.");

  if( m_MCPileupCheckContainer == "None" ) {
    m_useMCPileupCheck = false;
  }

  m_comEnergy = "13TeV";

  ANA_MSG_INFO("configure() : ResonanceAlgorithm Interface succesfully configured! \n");

  if( m_inJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : InputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  if( !m_truthLevelOnly &&  m_allJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : AllJetInputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ResonanceAlgorithm :: setupJob (EL::Job& job)
{

  ///////////////////////////////////////////////////////////////////////	
  // Here you put code that sets up the job on the submission object   //
  // so that it is ready to work with your algorithm, e.g. you can     //
  // request the D3PDReader service or add output files.  Any code you //
  // put here could instead also go into the submission script.  The   //
  // sole advantage of putting it here is that it gets automatically   //
  // activated/deactivated when you add/remove the algorithm from your //
  // job, which may or may not be of value to you.                     //
  ///////////////////////////////////////////////////////////////////////

  job.useXAOD();
  xAOD::Init( "ResonanceAlgorithm" ).ignore(); // call before opening first file

  EL::OutputStream outForTree( m_treeStream );
  job.outputAdd (outForTree);
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: histInitialize ()
{
  //////////////////////////////////////////////////////////////////////
  // Here you do everything that needs to be done at the very         //
  // beginning on each worker node, e.g. create histograms and output //
  // trees.  This method gets called before any input files are       //
  // connected.							      //
  //////////////////////////////////////////////////////////////////////

  ANA_MSG_INFO("histInitialize() : Calling histInitialize \n");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: fileExecute ()
{
  ////////////////////////////////////////////////////////////////////////
  // Here you do everything that needs to be done exactly once for every//  
  // single file, e.g. collect a list of all lumi-blocks processed      //
  ////////////////////////////////////////////////////////////////////////

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: changeInput (bool /*firstFile*/)
{
  ///////////////////////////////////////////////////////////////////////
  // Here you do everything you need to do when we change input files, //
  // e.g. resetting branch addresses on trees.  If you are using       //
  // D3PDReader or a similar service this method is not needed.        //
  ///////////////////////////////////////////////////////////////////////

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: initialize ()
{
  ///////////////////////////////////////////////////////////////////////
  // Here you do everything that you need to do after the first input  //
  // file has been connected and before the first event is processed,  //
  // e.g. create additional histograms based on which variables are    //
  // available in the input files.  You can also create all of your    //
  // histograms and trees in here, but be aware that this method       //
  // doesn't get called if no events are processed.  So any objects    //
  // you create here won't be available in the output if you have no   //
  // input events.						       //
  ///////////////////////////////////////////////////////////////////////


  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  m_eventCounter = -1;

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if ( this->configure() == EL::StatusCode::FAILURE ) {
    ANA_MSG_ERROR("initialize() : Failed to properly configure. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  // m_truthLevelOnly is set in config so need to do this after configure is called
  if( m_truthLevelOnly ) { m_isMC = true; }
  else {
    m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? true : false;
  }

  getLumiWeights(eventInfo);

  if(m_useCutFlow) {

    TFile *file = wk()->getOutputFile ("cutflow");
    m_cutflowHist  = (TH1D*)file->Get("cutflow");
    m_cutflowHistW = (TH1D*)file->Get("cutflow_weighted");

    // This algorithm only applies selections after the trigger is applied.
    // Here find the first bin that need to be filled: TriggerEfficiency 
    m_cutflowFirst = m_cutflowHist->GetXaxis()->FindBin("TriggerEfficiency");
    m_cutflowHistW->GetXaxis()->FindBin("TriggerEfficiency");

    if(m_useMCPileupCheck && m_isMC){
      m_cutflowHist->GetXaxis()->FindBin("mcCleaning");
      m_cutflowHistW->GetXaxis()->FindBin("mcCleaning");
    }

    m_cutflowHist->GetXaxis()->FindBin("y*");
    m_cutflowHistW->GetXaxis()->FindBin("y*");

  }

  ///////////////////////////////////////////////
  //              Append the btagging WPs      //
  ///////////////////////////////////////////////

  if (m_doBtag) {
    std::stringstream ss(m_bTagWPNames);
    std::string thisWPString;
    while (std::getline(ss, thisWPString, ',')) {
      m_bTagWPs.push_back( thisWPString );
    }
  }

  ANA_MSG_INFO("initialize() : Succesfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}

void ResonanceAlgorithm::AddTree( std::string name ) {

  std::string treeName("outTree");
  // naming convention
  treeName += name; // add systematic
  TTree * outTree = new TTree(treeName.c_str(),treeName.c_str());
  if( !outTree ) {
    ANA_MSG_ERROR("AddTree() : Failed to get output tree!");
  }
  TFile* treeFile = wk()->getOutputFile( m_treeStream );
  outTree->SetDirectory( treeFile );

  MiniTree* miniTree = new MiniTree(m_event, outTree, treeFile, m_store);
  // only limited information available in truth xAODs
  if( m_truthLevelOnly ) {
    miniTree->AddEvent("truth");
    miniTree->AddJets("kinematic");
  } else { // reconstructed xAOD
    miniTree->AddEvent( m_eventDetailStr );
    miniTree->AddTrigger( m_trigDetailStr );
    if( !name.empty() ) { // save limited information for systematic variations
      miniTree->AddJets( m_jetDetailStrSyst );
    } else {
      miniTree->AddJets( m_jetDetailStr );
    }
    if (m_doBtag){
      miniTree->AddBtag( m_bTagWPNames );
    }
  }
  m_myTrees[name] = miniTree;

}

void ResonanceAlgorithm::AddHists( std::string name ) {

  std::string jhName("highPtJets");
  jhName += name; // add systematic

}


EL::StatusCode ResonanceAlgorithm :: execute ()
{
  ////////////////////////////////////////////////////////////////////////
  // Here you do everything that needs to be done on every single       //
  // events, e.g. read input variables, apply cuts, and fill            //
  // histograms and trees.  This is where most of your actual analysis  //
  // code will go.							//
  ////////////////////////////////////////////////////////////////////////

  ANA_MSG_DEBUG("execute(): Applying selection");
  ++m_eventCounter;

  m_iCutflow = m_cutflowFirst;

  //----------------------------
  // Event information
  //---------------------------

  ///////////////////////////////////////////////////////////////////////////////
  ///////////                   Retrieve Containers                    //////////
  ///////////////////////////////////////////////////////////////////////////////

  ANA_MSG_DEBUG("execute() : Get Containers");
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if (m_eventCounter == 0 && m_isMC) {
    getLumiWeights(eventInfo);
  } else if (!m_isMC) {
    m_filtEff = m_xs = 1.0;
  }

  SG::AuxElement::ConstAccessor<float> NPVAccessor("NPV");
  const xAOD::VertexContainer* vertices = 0;
  if(!m_truthLevelOnly) {
    ANA_CHECK (HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));
  }
  if(!m_truthLevelOnly && !NPVAccessor.isAvailable( *eventInfo )) { // NPV might already be available
    eventInfo->auxdecor< float >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
  }

  const xAOD::JetContainer* truthJets = 0;
  if(m_useMCPileupCheck && m_isMC){
    ANA_CHECK (HelperFunctions::retrieve(truthJets, m_MCPileupCheckContainer, m_event, m_store));
  }

  //Set this first, as m_mcEventWeight is needed by passCut()
  if(m_isMC){
    m_mcEventWeight = eventInfo->mcEventWeight();
  }
  else{
    m_mcEventWeight = 1;
  }
    eventInfo->auxdecor< float >("weight_xs") = m_filtEff * m_xs;
    //cout << "xs" << m_xs << endl;
    //cout << "weight_xs" << m_filtEff * m_xs << endl;
    eventInfo->auxdecor< float >("weight") = m_mcEventWeight * m_xs * m_filtEff;

//}
  //float weight_pileup = 1.;
  //if( m_isMC && eventInfo->isAvailable< float >( "PileupWeight") )
  //  weight_pileup = eventInfo->auxdecor< float >("PileupWeight");

  ////////////////////////////////////////////////////////////////////////////////
  //                          Loop over Systematics                             //
  ////////////////////////////////////////////////////////////////////////////////

  ANA_MSG_DEBUG("execute() : Systematic Loop");

  // did any collection pass the cuts?
  bool pass(false);
  bool doCutflow(m_useCutFlow); // will only stay true for nominal
  const xAOD::JetContainer* signalJets = 0;
  const xAOD::JetContainer* allJets = 0;

  // if input comes from xAOD, or just running one collection,
  // then get the one collection and be done with it
  if( m_inputAlgo == "" || m_truthLevelOnly ) {
    ANA_CHECK (HelperFunctions::retrieve(signalJets, m_inJetContainerName, m_event, m_store));
    if(!m_truthLevelOnly) { ANA_CHECK (HelperFunctions::retrieve(allJets, m_allJetContainerName, m_event, m_store)); }

    // executeAnalysis
    pass = this->executeAnalysis( eventInfo, signalJets, truthJets, vertices, doCutflow, "" );

  }
  else { // get the list of systematics to run over

    // get vector of string giving the names
    std::vector<std::string>* systNames = 0;
    if ( m_store->contains< std::vector<std::string> >( m_inputAlgo ) ) {
      if(!m_store->retrieve( systNames, m_inputAlgo ).isSuccess()) {
        ANA_MSG_INFO("execute() : Cannot find vector from "<<m_inputAlgo.c_str());
        return StatusCode::FAILURE;
      }
    }


    // loop over systematics
    bool saveContainerNames(false);
    std::vector< std::string >* vecOutContainerNames = 0;
    if(saveContainerNames) { vecOutContainerNames = new std::vector< std::string >; }
    // shoudl only doCutflow for the nominal
    bool passOne(false);
    std::string inContainerName("");
    std::string allJetContainerName("");
    for( auto systName : *systNames ) {
      ANA_MSG_DEBUG("execute() : Systematic Loop"<<systName);
      inContainerName = m_inJetContainerName+systName;
      ANA_CHECK (HelperFunctions::retrieve(signalJets, inContainerName, m_event, m_store));
      //
      allJetContainerName = m_allJetContainerName+systName;
      ANA_CHECK (HelperFunctions::retrieve(allJets, allJetContainerName, m_event, m_store));
      //
      // allign with Dijet naming conventions
      if( systName.empty() ) { doCutflow = m_useCutFlow; } // only doCutflow for nominal
      else { doCutflow = false; }
      passOne = this->executeAnalysis( eventInfo, signalJets, truthJets, vertices, doCutflow, systName );
      // save the string if passing the selection
      if( saveContainerNames && passOne ) { vecOutContainerNames->push_back( systName ); }
      // the final decision - if at least one passes keep going!
      pass = pass || passOne;

    }

    // save list of systs that should be considered down stream
    if( saveContainerNames ) {
      ANA_CHECK (m_store->record( vecOutContainerNames, m_name));
    }
  }

  if(!pass) {
    wk()->skipEvent();
  }
  return EL::StatusCode::SUCCESS;

}

bool ResonanceAlgorithm :: executeAnalysis ( const xAOD::EventInfo* eventInfo,
    const xAOD::JetContainer* signalJets,
    const xAOD::JetContainer* truthJets,
    const xAOD::VertexContainer* vertices,
    bool doCutflow,
    std::string systName) {

  /////////////////////////////////////////////////////////////////////////////
  //                           Begin Selections                              //
  /////////////////////////////////////////////////////////////////////////////

  ////  leadingJetPt trigger efficiency BEFORE MCCLEANING ////
  if( signalJets->at(0)->pt() < m_leadingJetPtCut || signalJets->size() < m_jetMultiplicity){
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
  }

  //if(doCutflow) passCut(); //TriggerEfficiency
  
  if( signalJets->at(1)->pt() < m_subleadingJetPtCut || signalJets->size() < m_jetMultiplicity){
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
  }

  if(doCutflow) passCut(); //TriggerEfficiency

  //// mcCleaning AFTER TRIGGER EFFICIENCY////
  // for lower slices this cut prevents the leading jets from being pileup jets
  if(m_useMCPileupCheck && m_isMC && signalJets->size() > 1) {
    float pTAvg = ( signalJets->at(0)->pt() + signalJets->at(1)->pt() ) /2.0;
    if( truthJets->size() == 0 || (pTAvg / truthJets->at(0)->pt() > 1.4) ){
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
    }
    if(doCutflow) passCut(); //mcCleaning
  }

  if( m_yStarCut > 0 && signalJets->size() > 1 ) {
    eventInfo->auxdecor< float >( "yStar" ) = ( signalJets->at(0)->rapidity() - signalJets->at(1)->rapidity() ) / 2.0;
    if(fabs(signalJets->at(0)->rapidity() - signalJets->at(1)->rapidity())/2.0 < m_yStarCut){
      if(doCutflow) passCut(); //yStar
    }else{
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
    }
  }
  else if( m_yStarCut > 0){
    wk()->skipEvent();  return EL::StatusCode::SUCCESS;
  }
  else if ( m_yStarCut < 0){
    if(doCutflow) passCut();
  }

  ////////////////////////////////////////////////////////////////
  //                       End Selections                       //
  ////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //                        Add final variables                               //
  //////////////////////////////////////////////////////////////////////////////
  // why decorate even if might not be filling tree?
  // if are not set here, miniTree puts -999 since not available
  if( signalJets->size() > 1 ) {
    const xAOD::Jet* leadJet     = signalJets->at(0);
    const xAOD::Jet* subLeadJet  = signalJets->at(1);
   
    float mjj = ( leadJet->p4() + subLeadJet->p4() ).M() / 1000.;
    eventInfo->auxdecor< float >( "mjj" ) = mjj;

    bool m_reclusterJets = false;

    if (m_reclusterJets){
      unsigned int nReclusterJets = 2; //Only recluster into the first 2 jets

      //Get TLV for all jets we're reclustering into
      std::vector< TLorentzVector > rcJets;
      for(unsigned int iRCJet = 0; iRCJet < nReclusterJets; ++iRCJet){
        TLorentzVector tmpJet = TLorentzVector();
        tmpJet.SetPtEtaPhiE(signalJets->at(iRCJet)->pt(), signalJets->at(iRCJet)->eta(), signalJets->at(iRCJet)->phi(), signalJets->at(iRCJet)->e());
        rcJets.push_back( tmpJet );
      }

      for(unsigned int iJet = nReclusterJets; iJet < signalJets->size(); ++iJet){
        float minDR = 2.0;
        unsigned int minRCJet = 0; //the closest jet in dR

        for(unsigned int iRCJet = 0; iRCJet < nReclusterJets; ++iRCJet){
          float thisDR = signalJets->at(iJet)->p4().DeltaR( signalJets->at(iRCJet)->p4() );
          if (thisDR < minDR){
            minDR = thisDR;
            minRCJet = iRCJet;
          }
        }

        if(minDR < 1.2){
          rcJets.at(minRCJet) += signalJets->at(iJet)->p4();
        }
      }//recluster jets

      for(unsigned int iRCJet = 0; iRCJet < nReclusterJets; ++iRCJet){
        signalJets->at(iRCJet)->auxdecor< float >( "recluster_pt") = rcJets.at(iRCJet).Pt();
        signalJets->at(iRCJet)->auxdecor< float >( "recluster_eta") = rcJets.at(iRCJet).Eta();
        signalJets->at(iRCJet)->auxdecor< float >( "recluster_phi") = rcJets.at(iRCJet).Phi();
        signalJets->at(iRCJet)->auxdecor< float >( "recluster_e") = rcJets.at(iRCJet).E();
      }
    }//m_reclusterJets


    if (m_doBtag) {
      //%%%%%%%%%%%%%%%%%% b-tagged di-jet - START %%%%%%%%%%%%%%%%%%//

      std::vector<int> leadJet_bTag;
      std::vector<int> subLeadJet_bTag;
      
      for (unsigned int i=0; i<m_bTagWPs.size(); i++) {
        leadJet_bTag.push_back(leadJet->auxdecor< char >("BTag_"+m_bTagWPs.at(i)));
        subLeadJet_bTag.push_back(subLeadJet->auxdecor< char >("BTag_"+m_bTagWPs.at(i))); 
      }

      if(m_msgLevel == MSG::DEBUG) {
        if(leadJet_bTag.at(0) && subLeadJet_bTag.at(0)) {
        double leadJet_FT_weight(-9.);
        double subLeadJet_FT_weight(-9.);
        const xAOD::BTagging* leadJet_bTagObj = leadJet->btagging();
        if (leadJet_bTagObj) { leadJet_bTagObj->MVx_discriminant("DL1", leadJet_FT_weight); }
        const xAOD::BTagging* subLeadJet_bTagObj = subLeadJet->btagging();
        if (subLeadJet_bTagObj) { subLeadJet_bTagObj->MVx_discriminant("DL1", subLeadJet_FT_weight); }
        std::cout << "Event# " << m_eventCounter << " selected! " 
          << " lead pT " << leadJet->pt()
          << " / eta " << leadJet->eta() << " / DL1 " << leadJet_FT_weight
          << " subLead pT " << subLeadJet->pt()
          << " / eta " << subLeadJet->eta() << " / DL1 " << subLeadJet_FT_weight
          << std::endl;
        }
      }

      //get list of systematics values
      if( m_bTagSystematics.size() == 0 ){
        static SG::AuxElement::ConstAccessor< std::vector< std::string > > ac_systSF_btag_names("SystSF_btag_Names");
        if( ac_systSF_btag_names.isAvailable( *eventInfo ) ) { m_bTagSystematics = ac_systSF_btag_names( *eventInfo ); }
      }

      if (systName.empty()) {
        for (unsigned int iT=0; iT<m_bTagWPs.size(); iT++) {

          eventInfo->auxdecor< std::vector<float> >( "weight_BTag_"+m_bTagWPs.at(iT) ) = std::vector<float>();
          int numBTagSystematics = leadJet->auxdecor< std::vector<float> >("BTag_SF_"+m_bTagWPs.at(iT)).size();

          for(int iSys=0; iSys < numBTagSystematics; ++iSys){

            float leadJet_w = 1;
            float subLeadJet_w = 1;

            if( leadJet->isAvailable< std::vector<float> >( "BTag_SF_"+m_bTagWPs.at(iT) )) 
              leadJet_w = leadJet->auxdecor< std::vector<float> >("BTag_SF_"+m_bTagWPs.at(iT)).at(iSys);
            if( subLeadJet->isAvailable< std::vector<float> >( "BTag_SF_"+m_bTagWPs.at(iT) ))
              subLeadJet_w = subLeadJet->auxdecor< std::vector<float> >("BTag_SF_"+m_bTagWPs.at(iT)).at(iSys);

            eventInfo->auxdecor< std::vector<float> >( "weight_BTag_"+m_bTagWPs.at(iT) ).push_back( leadJet_w*subLeadJet_w);
          }
        }
      }
      //%%%%%%%%%%%%%%%%%% b-tagged di-jet - END %%%%%%%%%%%%%%%%%%//
    }

    eventInfo->auxdecor< float >( "yStar" ) = ( signalJets->at(0)->rapidity() - signalJets->at(1)->rapidity() ) / 2.0;
    eventInfo->auxdecor< float >( "yBoost" ) = ( leadJet->rapidity() + subLeadJet->rapidity() ) / 2.0;

    // add counter of how many of the leading two jets are punch-through
    static SG::AuxElement::ConstAccessor<int> muonSegCount ("GhostMuonSegmentCount");
    int punch_type_segs = -1;
    // flip if sub-leading doesn't punchthrough but leading does
    float Insitu_Segs_response_E  = subLeadJet->e()  / leadJet->e();
    float Insitu_Segs_response_pT = subLeadJet->pt() / leadJet->pt();
    if( muonSegCount.isAvailable( *leadJet ) && muonSegCount.isAvailable( *subLeadJet ) ) {
      if( !muonSegCount( *leadJet ) && !muonSegCount( *subLeadJet ) ) {  // both don't punchthrough
        punch_type_segs = 0;
      } else if( !muonSegCount( *leadJet ) ) { // leading doesn't punchthrough
        punch_type_segs = 1;
      } else if( !muonSegCount( *subLeadJet ) ) { // sub-leading doesn't punchthrough
        punch_type_segs = 2;
        Insitu_Segs_response_E  = 1.0 / Insitu_Segs_response_E;
        Insitu_Segs_response_pT = 1.0 / Insitu_Segs_response_pT;
      } else {
        punch_type_segs = 3;
      }
    }

    eventInfo->auxdecor< float >( "Insitu_Segs_response_E" )  = Insitu_Segs_response_E;
    eventInfo->auxdecor< float >( "Insitu_Segs_response_pT" ) = Insitu_Segs_response_pT;
    eventInfo->auxdecor< int >("punch_type_segs") = punch_type_segs;

  } // 2 or more jets

  if( !m_truthLevelOnly ) {
    TLorentzVector MHT = TLorentzVector(0.0, 0.0, 0.0, 0.0);
    TLorentzVector MHTJVT = TLorentzVector(0.0, 0.0, 0.0, 0.0);
    for( auto iJet : *signalJets) {
      MHT -= iJet->p4();
      if( iJet->pt() < 60e3 && fabs(iJet->getAttribute<xAOD::JetFourMom_t>("JetEMScaleMomentum").eta()) < 2.4 ){
	      if( iJet->getAttribute< float >( "Jvt" ) < 0.59 ) {
		      continue;
	      }
      }
      MHTJVT -= iJet->p4();
    } // loop over all jets
    eventInfo->auxdecor< float >( "MHT"    )    = ( MHT.Pt() / 1000. );
    eventInfo->auxdecor< float >( "MHTPhi" )    = ( MHT.Phi() );
    eventInfo->auxdecor< float >( "MHTJVT" )    = ( MHTJVT.Pt() / 1000. );
    eventInfo->auxdecor< float >( "MHTJVTPhi" ) = ( MHTJVT.Phi() );
  }

  // detector eta and punch-through-variable
  if( !m_truthLevelOnly ) {
    for( auto iJet : *signalJets ) {
      xAOD::JetFourMom_t jetConstitScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
      xAOD::JetFourMom_t jetEMScaleP4 = iJet->getAttribute<xAOD::JetFourMom_t>("JetEMScaleMomentum");
      iJet->auxdecor< float >( "constitScaleEta") = jetConstitScaleP4.eta();
      iJet->auxdecor< float >( "emScaleEta")      = jetEMScaleP4.eta();
      iJet->auxdecor< float >( "emScalePhi")      = jetEMScaleP4.phi();
      iJet->auxdecor< float >( "emScaleE")        = jetEMScaleP4.E();
    }

  }

  ANA_MSG_DEBUG("Event # "<< m_eventCounter);

  /////////////////////////////////////// Output Plots ////////////////////////////////
  //float weight(1);
  //if( eventInfo->isAvailable< float >( "weight" ) ) {
  //  weight = eventInfo->auxdecor< float >( "weight" );
  //}

  ///////////////////////////// fill the tree ////////////////////////////////////////////
  if(m_writeTree){
    if (m_myTrees.find( systName ) == m_myTrees.end() ) { AddTree(systName); }
    if(eventInfo) { m_myTrees[systName]->FillEvent(eventInfo, m_event); }
    if( m_truthLevelOnly ) {
      if(signalJets)  { m_myTrees[systName]->FillJets( signalJets, -1 ); }
    } else {
      m_myTrees[systName]->FillTrigger( eventInfo );
      if(signalJets)  m_myTrees[systName]->FillJets( signalJets, HelperFunctions::getPrimaryVertexLocation( vertices ));
      if(m_doBtag) m_myTrees[systName]->FillBtag( eventInfo );
      //if(m_doBtag) m_myTrees[systName]->FillBtagHighEff( eventInfo );
    }
    m_myTrees[systName]->Fill();
  }

  return true;
}

//Easy method for automatically filling cutflow and incrementing counter
void ResonanceAlgorithm::passCut() {
  m_cutflowHist->Fill(m_iCutflow, 1);
  m_cutflowHistW->Fill(m_iCutflow, m_mcEventWeight);
  m_iCutflow++;
}

//This grabs cross section, acceptance, and eventNumber information from the respective text file
//text format:     147915 2.3793E-01 5.0449E-03 499000
EL::StatusCode ResonanceAlgorithm::getLumiWeights(const xAOD::EventInfo* eventInfo) {

  if(!m_isMC){
    m_mcChannelNumber = eventInfo->runNumber();
    m_xs = 1;
    m_filtEff = 1;
    m_numAMIEvents = 0;
    return EL::StatusCode::SUCCESS;
  }

  m_mcChannelNumber = eventInfo->mcChannelNumber();
  //if mcChannelNumber = 0 need to retrieve from runNumber
  if(eventInfo->mcChannelNumber()==0)  m_mcChannelNumber = eventInfo->runNumber();
  ifstream fileIn(  gSystem->ExpandPathName( ("$WorkDir_DIR/data/MultiBTaggedAlgo/XsAcc_"+m_comEnergy+".txt").c_str() ) );
  std::string runNumStr = std::to_string( m_mcChannelNumber );
  std::string line;
  std::string subStr;
  while (getline(fileIn, line)){
    istringstream iss(line);
    iss >> subStr;
    if (subStr.find(runNumStr) != string::npos){
      iss >> subStr;
      sscanf(subStr.c_str(), "%e", &m_xs);
      iss >> subStr;
      sscanf(subStr.c_str(), "%e", &m_filtEff);
      iss >> subStr;
      sscanf(subStr.c_str(), "%i", &m_numAMIEvents);
      cout << "Setting xs / acceptance / numAMIEvents to " << m_xs << ":" << m_filtEff << ":" << m_numAMIEvents << endl;
      continue;
    }
  }
  if( m_numAMIEvents == 0){
    cerr << "ERROR: Could not find proper file information for file number " << runNumStr << endl;
    return EL::StatusCode::FAILURE;
  }
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ResonanceAlgorithm :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  if( m_writeTree ) {
    std::string thisName;
    //m_ss.str( std::string() );
    //m_ss << m_mcChannelNumber;
    TFile * treeFile = wk()->getOutputFile( m_treeStream );
    if(m_useCutFlow) {
      TH1F* thisCutflowHist = (TH1F*) m_cutflowHist->Clone();
      thisName = thisCutflowHist->GetName();
      thisCutflowHist->SetName( (thisName).c_str() );
      //thisCutflowHist->SetName( (thisName+"_"+m_ss.str()).c_str() );
      thisCutflowHist->SetDirectory( treeFile );

      TH1F* thisCutflowHistW = (TH1F*) m_cutflowHistW->Clone();
      thisName = thisCutflowHistW->GetName();
      thisCutflowHistW->SetName( (thisName).c_str() );
      //thisCutflowHistW->SetName( (thisName+"_"+m_ss.str()).c_str() );
      thisCutflowHistW->SetDirectory( treeFile );
    }
    // Get MetaData_EventCount histogram
    TFile* metaDataFile = wk()->getOutputFile( "metadata" );
    TH1D* metaDataHist = (TH1D*) metaDataFile->Get("MetaData_EventCount");
    TH1D* thisMetaDataHist = (TH1D*) metaDataHist->Clone();
    thisMetaDataHist->SetDirectory( treeFile );
  }

  return EL::StatusCode::SUCCESS;
}
