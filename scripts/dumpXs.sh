#!/bin/bash

export DSID=401301
export XSFB=1.8255E+03
export ACC=1.0000E+00
export NEVTOT=40000
export NAME=myQstar

XSFILE="MultiBTaggedAlgo/data/XsAcc_13TeV.txt"
HELP="dumpXs.sh -d DSID -x XSFB -a ACC -n NEV -t TAG"

while getopts h:d:x:a:n:t: option
do
case "${option}"
in
d) DSID=${OPTARG};;
x) XSFB=${OPTARG};;
a) ACC=${OPTARG};;
n) NEVTOT=${OPTARG};;
t) NAME=${OPTARG};;
h) (echo $HELP && exit 1);;
esac
done

echo "Need to add XS for DSID $DSID"
INLINE="$DSID $XSFB $ACC $NEVTOT"
if grep -Fq "$DSID" "$XSFILE"
then
    FOUNDLINE=`grep -F "$DSID" "$XSFILE"`
    OLDLINE=${FOUNDLINE%" #"*}
else
    FOUNDLINE=""
    OLDLINE=""
fi

if [ -z "$FOUNDLINE" ]
then
    echo "$DSID not found hence adding it to $XSFILE"
    echo "$DSID $XSFB $ACC $NEVTOT #$NAME" >> $XSFILE
else
    echo "$DSID already listed in $XSFILE" 
    if [ "$INLINE" != "$OLDLINE" ]
    then
	echo "The content is different from what expected, replacing:"
	echo $OLDLINE "with" $INLINE
	sed -i "s/$OLDLINE/$INLINE/g" $XSFILE
    fi
fi
cat $XSFILE |grep "$DSID"
