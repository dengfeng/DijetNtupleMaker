#!/bin/bash

echo "Hello, world"

TESTFILE="DxAOD.root"
NEVENTS=-1
ISDATA=false
ISSIG=false
ISBKG=false
while getopts dsbf:n: option
do 
case "${option}"
in
f) TESTFILE=${OPTARG};;
d) ISDATA=true;;
s) ISSIG=true;;
b) ISBKG=true;;
n) NEVENTS=${OPTARG};;
esac
done


export ATLAS_LOCAL_ROOT_BASE=/home/atlas/
mkdir /usr/multib/run && cd /usr/multib/run && echo $(pwd) && ls -ltrh

EOSREPO="root://eoshome.cern.ch//eos/user/d/dibijet/DAOD/"
#TESTFILE="/data17_13TeV.00333367.physics_Main.deriv.DAOD_EXOT2.f857_m1855_p3372/DAOD_EXOT2.12622470._000033.pool.root.1"
LOCALFILE="DxAOD.root"
TESTDIR="localTest"

echo "copying file from eos"


echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH

if [ ! -f ${LOCALFILE} ]; then
    echo "File ${EOSREPO}${LOCALFILE} not found! Retriving it from EOS"
    echo xrdcp ${EOSREPO}${TESTFILE} ${LOCALFILE}
    xrdcp ${EOSREPO}${TESTFILE} ${LOCALFILE}
fi

if [ $ISDATA = true ]; then
    CONFIGFILE="/usr/multib/src/MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_Nominal_2017.py"
elif [ $ISSIG = true ]; then
    CONFIGFILE="/usr/multib/src/MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_signal.py"
elif [ $ISBKG = true ]; then
    CONFIGFILE="/usr/multib/src/MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_QCD.py"
fi

echo "Config file: ${CONFIGFILE}"

source /usr/multib/build/${AnalysisBase_PLATFORM}/setup.sh

if [ $ISDATA ]; then
    echo xAH_run.py --config ${CONFIGFILE} --files ${LOCALFILE} --submitDir ${TESTDIR} --nevents ${NEVENTS} direct
    xAH_run.py --config ${CONFIGFILE} --files ${LOCALFILE} --submitDir ${TESTDIR} --nevents ${NEVENTS} direct
else
    echo xAH_run.py --config ${CONFIGFILE} --files ${LOCALFILE} --isMC --submitDir ${TESTDIR} --nevents ${NEVENTS} direct
    xAH_run.py --config ${CONFIGFILE} --files ${LOCALFILE} --isMC --submitDir ${TESTDIR} --nevents ${NEVENTS} direct
fi

ls -ltrh
ls -ltrh ${TESTDIR}

if [ -f ${LOCALFILE} ]; then
    rm -r ${LOCALFILE}
fi

echo "That's all, folks!"

