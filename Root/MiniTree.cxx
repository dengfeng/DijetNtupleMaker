#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODAnaHelpers/HelperFunctions.h"

#include "MultiBTaggedAlgo/MiniTree.h"

MiniTree :: MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, xAOD::TStore* store /* = 0 */) :
  HelpTreeBase(event, tree, file, 1e3)
{
  Info("MiniTree", "Creating output TTree");
  tree->SetAutoSave(1000000000);
  m_firstEvent = true;
  m_store = store;
}

MiniTree :: ~MiniTree()
{
}
/////////////////////////////////////////////////////////////
///////////   Add user defined event variables             //
/////////////////////////////////////////////////////////////


void MiniTree::AddEventUser(const std::string detailStr)
{
  // punch-through
  m_tree->Branch("Insitu_Segs_response_E", &m_Insitu_Segs_response_E, "Insitu_Segs_response_E/F");
  m_tree->Branch("Insitu_Segs_response_pT", &m_Insitu_Segs_response_pT, "Insitu_Segs_response_pT/F");
  m_tree->Branch("punch_type_segs", &m_punch_type_segs, "punch_type_segs/I");

  m_tree->Branch("MHT",       &m_MHT,       "MHT/F");
  m_tree->Branch("MHTPhi",    &m_MHTPhi,    "MHTPhi/F");
  m_tree->Branch("MHTJVT",    &m_MHTJVT,    "MHTJVT/F");
  m_tree->Branch("MHTJVTPhi", &m_MHTJVTPhi, "MHTJVTPhi/F");

  // weights
  m_tree->Branch("weight", &m_weight, "weight/F");
  m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");
  m_tree->Branch("weight_prescale", &m_weight_prescale, "weight_prescale/F");
  m_tree->Branch("weight_resonanceKFactor", &m_weight_resonanceKFactor, "weight_resonanceKFactor/F");
 
  // yBoost
  m_tree->Branch("yBoost", &m_yBoost, "yBoost/F");
  m_tree->Branch("yStar", &m_yStar, "yStar/F");
  m_tree->Branch("mjj", &m_mjj, "mjj/F");

}

/////////////////////////////////////////////////////////////
//////////   Add user defined jet variables             /////
/////////////////////////////////////////////////////////////

void MiniTree::AddJetsUser(const std::string detailStr, const std::string jetName)
{
  m_tree->Branch("jet_constitScaleEta", &m_jet_constitScaleEta);
  m_tree->Branch("jet_emScaleE", &m_jet_emScaleE);
  m_tree->Branch("jet_emScaleEta", &m_jet_emScaleEta);
  m_tree->Branch("jet_emScalePhi", &m_jet_emScalePhi);

  //reclustered jets
  m_tree->Branch("jet_recluster_pt", &m_jet_recluster_pt);
  m_tree->Branch("jet_recluster_eta", &m_jet_recluster_eta);
  m_tree->Branch("jet_recluster_phi", &m_jet_recluster_phi);
  m_tree->Branch("jet_recluster_e", &m_jet_recluster_e);
}

/////////////////////////////////////////////////////////////////
/////    Clear user defined event variables  ////////////////////
/////////////////////////////////////////////////////////////////

void MiniTree::ClearEventUser() {
 
  m_Insitu_Segs_response_E = -999;
  m_Insitu_Segs_response_pT = -999;
  m_punch_type_segs = -999;

  m_MHT      = -999;
  m_MHTJVT   = -999;
  m_MHTPhi   = -999;
  m_MHTJVTPhi= -999;

  m_weight_corr = -999;
  m_weight    = -999;
  m_weight_xs = -999;
  m_weight_prescale = -999;
  m_weight_resonanceKFactor = -999;

  m_yBoost = -999;
  m_yStar = -999;
  m_mjj = -999;
}

////////////////////////////////////////////////////////////
////      Clear user defined jet variables                //
////////////////////////////////////////////////////////////

void MiniTree::ClearJetsUser(const std::string jetName) {
  m_jet_constitScaleEta.clear();
  m_jet_emScaleE.clear();
  m_jet_emScaleEta.clear();
  m_jet_emScalePhi.clear();

  //reclustered jets
  m_jet_recluster_pt.clear();
  m_jet_recluster_eta.clear();
  m_jet_recluster_phi.clear();
  m_jet_recluster_e.clear();
}

//////////////////////////////////////////////////////////////////
//               Fill defined event variables                   //
//////////////////////////////////////////////////////////////////

void MiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {
  if( eventInfo->isAvailable< float >( "weight" ) )
    m_weight = eventInfo->auxdecor< float >( "weight" );
  if( eventInfo->isAvailable< float >( "weight_xs" ) )
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );
  if( eventInfo->isAvailable< float >( "weight_prescale" ) )
    m_weight_prescale = eventInfo->auxdecor< float >( "weight_prescale" );
  if( eventInfo->isAvailable< float >( "weight_resonanceKFactor" ) )
    m_weight_resonanceKFactor = eventInfo->auxdecor< float >( "weight_resonanceKFactor" );

  if( eventInfo->isAvailable< float >( "yBoost" ) )
    m_yBoost = eventInfo->auxdecor< float >( "yBoost" );
  if( eventInfo->isAvailable< float >( "yStar" ) )
    m_yStar = eventInfo->auxdecor< float >( "yStar" );
  if( eventInfo->isAvailable< float >( "mjj" ) )
    m_mjj = eventInfo->auxdecor< float >( "mjj" );

  if( eventInfo->isAvailable< float >( "Insitu_Segs_response_E" ) )
    m_Insitu_Segs_response_E = eventInfo->auxdecor< float >( "Insitu_Segs_response_E" );
  if( eventInfo->isAvailable< float >( "Insitu_Segs_response_pT" ) )
    m_Insitu_Segs_response_pT = eventInfo->auxdecor< float >( "Insitu_Segs_response_pT" );
  if( eventInfo->isAvailable< int >( "punch_type_segs" ) )
    m_punch_type_segs = eventInfo->auxdecor< int >( "punch_type_segs" );


  if( eventInfo->isAvailable< float >( "MHT" ) )
	  m_MHT = eventInfo->auxdecor< float >( "MHT" );
  if( eventInfo->isAvailable< float >( "MHTPhi" ) )
	  m_MHTPhi = eventInfo->auxdecor< float >( "MHTPhi" );
  if( eventInfo->isAvailable< float >( "MHTJVT" ) )
	  m_MHTJVT = eventInfo->auxdecor< float >( "MHTJVT" );
  if( eventInfo->isAvailable< float >( "MHTJVTPhi" ) )
	  m_MHTJVTPhi = eventInfo->auxdecor< float >( "MHTJVTPhi" );

}

///////////////////////////////////////////////////////////////////
//             Fill user defined jet variables                   //
///////////////////////////////////////////////////////////////////

void MiniTree::FillJetsUser( const xAOD::Jet* jet, const std::string jetName ) {
  if( jet->isAvailable< float >( "constitScaleEta" ) ) {
    m_jet_constitScaleEta.push_back( jet->auxdata< float >("constitScaleEta") );
  } else {
    m_jet_constitScaleEta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "emScaleE" ) ) {
    m_jet_emScaleE.push_back( jet->auxdata< float >("emScaleE") );
  } else {
    m_jet_emScaleE.push_back( -999 );
  }
  if( jet->isAvailable< float >( "emScaleEta" ) ) {
    m_jet_emScaleEta.push_back( jet->auxdata< float >("emScaleEta") );
  } else {
    m_jet_emScaleEta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "emScalePhi" ) ) {
    m_jet_emScalePhi.push_back( jet->auxdata< float >("emScalePhi") );
  } else {
    m_jet_emScalePhi.push_back( -999 );
  }

  //reclustered jets
  if( jet->isAvailable< float >( "recluster_pt" ) ) {
    m_jet_recluster_pt.push_back( jet->auxdata< float >("recluster_pt") / 1e3 );
  } else {
    m_jet_recluster_pt.push_back( -999 );
  }
  if( jet->isAvailable< float >( "recluster_eta" ) ) {
    m_jet_recluster_eta.push_back( jet->auxdata< float >("recluster_eta") );
  } else {
    m_jet_recluster_eta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "recluster_phi" ) ) {
    m_jet_recluster_phi.push_back( jet->auxdata< float >("recluster_phi") );
  } else {
    m_jet_recluster_phi.push_back( -999 );
  }
  if( jet->isAvailable< float >( "recluster_e" ) ) {
    m_jet_recluster_e.push_back( jet->auxdata< float >("recluster_e") / 1e3 );
  } else {
    m_jet_recluster_e.push_back( -999 );
  }

}

void MiniTree::AddBtag(std::string bTagWPNames) {
    std::stringstream ss(bTagWPNames);
    std::string thisWPString;

    // Split list of WPs by commas, and make vector placeholders //
    while (std::getline(ss, thisWPString, ',')) {
      m_bTagWPs.push_back( thisWPString );
      /*if (thisWPString.find("Fixed") != std::string::npos)
        m_bTagIsFixed.push_back(true);
      else
        m_bTagIsFixed.push_back(false);

      std::string thisNum = thisWPString.substr( thisWPString.find_last_of('_')+1, thisWPString.size() );
      std::string thisTagger = thisWPString.substr( 0, thisWPString.find_first_of('_')-1);
      
      m_bTagWPNums.push_back(thisNum);
      m_bTaggers.push_back(thisTagger);
      */ 
     std::vector<float>  tmpVec;
     m_weight_btag.push_back( tmpVec );
    }  

    /*
    sort( m_bTagWPNums.begin(), m_bTagWPNums.end() );
    m_bTagWPNums.erase( unique( m_bTagWPNums.begin(), m_bTagWPNums.end() ), m_bTagWPNums.end() );
    sort( m_bTaggers.begin(), m_bTaggers.end() );
    m_bTaggers.erase( unique( m_bTaggers.begin(), m_bTaggers.end() ), m_bTaggers.end() );
    */

    // Connect vector placeholders to WPs //
    for(unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
     /* for (unsigned int iN=0; iN < m_bTagWPNums.size(); ++iN) {
        std::string baseBTagType = "";
        if (m_bTagIsFixed.at(iB))
          baseBTagType = "fix_";
        else
          baseBTagType = "flt_";

     */
      std::string thisBTagName = "";
      thisBTagName = "weight_btag_"+m_bTagWPs.at(iB);
      m_tree->Branch(thisBTagName.c_str(), &(m_weight_btag.at(iB)) );  
    }
  
    m_tree->Branch("systSF_btag_names",      &m_systSF_btag_names    );

}
/////////////////// Add variables for the b-tagged di-jet analysis //////////////////
void MiniTree::FillBtag( const xAOD::EventInfo* eventInfo ) {
  this->ClearBtag();

  // Find and fill relevant varaibles for all WPs //
  for( unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
    /*
    std::string baseBTagType = "";
    if (m_bTagIsFixed.at(iB))
      baseBTagType = "MV2c10_FixedCutBEff_";
    else
      baseBTagType = "FlatBEff_";
    */
    std::string thisBTagName = "";
    thisBTagName = "weight_BTag_"+m_bTagWPs.at(iB);
    if( eventInfo->isAvailable< std::vector< float > >( thisBTagName.c_str() ) )
      m_weight_btag.at(iB) = eventInfo->auxdecor< std::vector< float > >( thisBTagName.c_str() );
  }

  // get vector of string giving the syst names of the upstream algo from TStore
  // (rememeber: 1st element is a blank string: nominal case!)
  if( m_firstEvent){
    std::vector< std::string >* systNames(nullptr);
    if( m_store->contains<std::vector<std::string> >( "BJetEfficiency_Algo_DL1_FixedCutBEff_85" )){
      HelperFunctions::retrieve(systNames, "BJetEfficiency_Algo_DL1_FixedCutBEff_85", 0, m_store); //.isSuccess()
      std::cout << " Using " <<  systNames->size() << " systematics from BJetEfficiency_Algo_DL1_FixedCutBEff_85 " << std::endl;
    }else if( m_store->contains<std::vector<std::string> >( "BJetEfficiency_Algo_DL1_FixedCutBEff_77" )){
      HelperFunctions::retrieve(systNames, "BJetEfficiency_Algo_DL1_FixedCutBEff_77", 0, m_store); //.isSuccess()
      std::cout << " Using " << systNames->size() << " systematics from BJetEfficiency_Algo_DL1_FixedCutBEff_77 " << std::endl;
    }else if( m_store->contains<std::vector<std::string> >( "BJetEfficiency_Algo_DL1r_FixedCutBEff_85" )){
      HelperFunctions::retrieve(systNames, "BJetEfficiency_Algo_DL1r_FixedCutBEff_85", 0, m_store); //.isSuccess()
      std::cout << " Using " << systNames->size() << " systematics from BJetEfficiency_Algo_DL1r_FixedCutBEff_85 " << std::endl;
    }else if( m_store->contains<std::vector<std::string> >( "BJetEfficiency_Algo_DL1r_FixedCutBEff_77" )){
      HelperFunctions::retrieve(systNames, "BJetEfficiency_Algo_DL1r_FixedCutBEff_77", 0, m_store); //.isSuccess()
      std::cout << " Using " << systNames->size() << " systematics from BJetEfficiency_Algo_DL1r_FixedCutBEff_77 " << std::endl;
    } else {
      std::cout << " No b-tagging systematics found !!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }

    m_systSF_btag_names = *systNames;
    if( m_systSF_btag_names.size() > 0 && m_systSF_btag_names.at(0).size() == 0)
      m_systSF_btag_names.at(0) = "Nominal";

    m_firstEvent = false;
  }
}

void MiniTree::ClearBtag() {

  for( unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
    m_weight_btag.at(iB).clear();
  }
  m_systSF_btag_names.clear();
}
