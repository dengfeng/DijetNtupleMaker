#!/bin/bash

echo "Hello, world"

echo "copying file from eos"

export ATLAS_LOCAL_ROOT_BASE=/home/atlas/
mkdir /usr/multib/run && cd /usr/multib/run && echo $(pwd) && ls -ltrh

EOSREPO="root://eoshome.cern.ch//eos/user/d/dibijet/DAOD/"
TESTFILE="/data17_13TeV.00333367.physics_Main.deriv.DAOD_EXOT2.f857_m1855_p3372/DAOD_EXOT2.12622470._000033.pool.root.1"
LOCALFILE="DxAOD.root"
TESTDIR="localTest"

echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH

if [ ! -f ${LOCALFILE} ]; then
    echo "File ${LOCALFILE} not found! Retriving it from EOS"
    echo xrdcp ${EOSREPO}${TESTFILE} ${LOCALFILE}
    xrdcp ${EOSREPO}${TESTFILE} ${LOCALFILE}
fi

CONFIGFILE="/usr/multib/src/MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_Nominal_2017.py"

echo "Config file: ${CONFIGFILE}"

source /usr/multib/build/${AnalysisBase_PLATFORM}/setup.sh

echo xAH_run.py --config ${CONFIGFILE} --files ${LOCALFILE} --submitDir ${TESTDIR} --nevents 3000 direct
xAH_run.py --config ${CONFIGFILE} --files ${LOCALFILE} --submitDir ${TESTDIR} --nevents 3000 direct

ls -ltrh
ls -ltrh ${TESTDIR}

if [ -f ${LOCALFILE} ]; then
    rm -r ${LOCALFILE}
fi

echo "That's all, folks!"

