################################################################################
# Package: DijetResonanceAlgo
################################################################################

# Declare the package name:
atlas_subdir( MultiBTaggedAlgo )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthToolSupport/AsgTools
                          Control/xAODRootAccess
                          Event/xAOD/xAODEventInfo
                          Event/xAOD/xAODJet
                          Event/xAOD/xAODMissingET
                          PhysicsAnalysis/D3PDTools/EventLoop
                          PhysicsAnalysis/D3PDTools/RootCoreUtils
                          xAODAnaHelpers
                          PRIVATE
                          Event/xAOD/xAODEgamma
                          Event/xAOD/xAODMuon )

# external dependencies
find_package( ROOT COMPONENTS Core RIO Hist Tree )

# build a dictionary for the library
atlas_add_root_dictionary ( MultiBTaggedAlgoLib MultiBTaggedAlgoDictSource
                            ROOT_HEADERS MultiBTaggedAlgo/*.h Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT )

# Component(s) in the package:
atlas_add_library( MultiBTaggedAlgoLib MultiBTaggedAlgo/*.h Root/*.h Root/*.cxx ${MultiBTaggedAlgoDictSource}
  PUBLIC_HEADERS MultiBTaggedAlgo
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
  LINK_LIBRARIES ${ROOT_LIBRARIES}
  AsgTools
  EventLoop
  xAODAnaHelpersLib
  xAODEgamma
  xAODEventInfo
  xAODJet
  xAODMissingET
  xAODMuon
  xAODRootAccess )

# Install files from the package:
atlas_install_scripts( scripts/*.py )
atlas_install_data( data/*.py data/*.xml data/*.txt data/*.root scripts/sampleLists/*)
